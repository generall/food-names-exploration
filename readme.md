# Products exploration


This repo contains the EDA of product data. The data consists of ~50k small titles of food items with an optional description.
Each item has a vendor, defined by ID.

You can enable BitBucket ipynb web-viewer in your profile settings -> App Marketplace -> Notebook Viewer -> Add.

## Usage

Install requirements
```
pip install -r requirements.txt
```

## Content

* [explore_data.ipynb](explore_data.ipynb)
    * Downloads data
    * Shows some very basic statistics of the dataset 
    * Builds vocabulaty
* [data_cleaning.ipynb](data_cleaning.ipynb)
    * Contains set of tools to clean text data
* [words_clustering.ipynb](words_clustering.ipynb)
    * Explore clusters, created by single word presence feature
    * T-SNE on word-embeddings were used to plot close items on a 2d plane
* [w2v_clustering.ipynb](w2v_clustering.ipynb)
    * Explore clustering, based on Word Mover's Distance
    * T-SNE on word-embeddings were used to plot close items on a 2d plane

## Key points

* Since we don't have any purpose for clustering except for gathering an idea of the data, we don't need to select any particular model. We just can use all of them to obtain as much intuition as possible.
* Simple clustering could be defined directly from the used vocabulary. For example, cooking methods.
* Words based clusters are small and top-20 clusters cover less than 50% off all data
* The main reasons for the formation of clusters:
    * Common vendor (e.g. Subway)
    * Common product (e.g. tea, coffee)
    * Common product feature (Italian, spacy)

## Possible improvements

* Extend sample size, use better embeddings
    * Train own embeddings on products data if there are enough products
* Try to use descriptions for clustering.
    * Use full-text matching for finding close descriptions
* Train doc-to-vec models for better title embeddings.
* Try to improve vocab-based clustering by following steps:
    * Apply NER to determine which word defines what property. Example: (`blueberry cheesecake slice`) -> `blueberry` - ingredients, `cheesecake` - product, `slice` - product form. After that, we would be able to compare only matching parts of the names.
    * Cluster vocabulary words to extend possible clusters. Example: `fish` -> (`salmon`, `tuna`, `codfish`)
    * By following these steps, we will be able to group food items into product groups.